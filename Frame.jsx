import React from 'react';

import HeaderNav from './partials/HeaderNav';
import SectionWelcome from './partials/SectionWelcome';
import SectionTab from './partials/SectionTab';
import SectionBlockStartup from './partials/SectionBlockStartup';
import SectionDirectory from './partials/SectionDirectory';
import SectionFooter from './partials/SectionFooter';

module.exports = class Frame extends React.Component
{
	constructor(props)
	{
		super(props)
	}
	render()
	{
		return(
			<html>
				<head>
					<title>Launchpad</title>
				    <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
				    <meta name="viewport" content="width=device-width, initial-scale=1"/>
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css"/>
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

					<link rel="stylesheet" href="assets/css/slider.css"/>
					<link rel="stylesheet" href="assets/css/compiledSass.css"/>
				</head>
				<body>
					<HeaderNav/>
					<SectionWelcome/>
					<SectionTab/>
					<SectionBlockStartup/>
					<SectionDirectory/>
					<SectionFooter/>

					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

					<script src="assets/js/scroll.js"></script>
					<script src="assets/js/webpack-bundle/frame.bundle.js"></script>

				</body>
			</html>
		)
	}
};
