import React from 'react';
import companies from '../data/company';
import _ from 'lodash';

class SectionDirectory extends React.Component {
  constructor(props) {
    super(props);
    this.itemPerPage = 12;
    this.state = {
      page: 1,
      listAll: companies,
      companies: companies.slice(0, this.itemPerPage),
    };
    this.inputFilters = {
      name: null,
      block: null,
      type:null,
    };
  }

  goToPage(page) {
    this.setState({
      page: page,
      companies: this.state.listAll.slice(this.itemPerPage * (page - 1), this.itemPerPage * page),
    });
  }

  setNewList(newList) {
    this.setState({
      page: 1,
      listAll: newList,
      companies: newList.slice(0, this.itemPerPage),
    });
  }

  filter(e) {
    const newList = companies.filter((company) =>
      company.name.toLowerCase().includes(this.inputFilters.name.value.toLowerCase()) &&
      company.type.includes(this.inputFilters.type.value) &&
      company.block.includes(this.inputFilters.block.value)
    );
    this.setNewList(newList);
  }

  getSimpleCompanyName(str) {
    return str.replace(/\s+/g, '').toLowerCase().replace(/pte.ltd./g,'');
  }

  render() {
    const lastPage = Math.ceil(this.state.listAll.length/this.itemPerPage);
    const pageStart = (this.state.page - 2) < 1  ? 1 : (this.state.page - 2);
    const pageEnd = (this.state.page + 2) > lastPage  ? lastPage : (this.state.page + 2);

    return (
      <div className="container">
        <h1>Directory</h1>
        <div className="row">
          <input type="text" name="directory-search" id="directory-search" ref={input => this.inputFilters.name = input} onChange={() => this.filter()} />
          <div className="pull-right">
            <div className="col-lg-6">
              <select className="form-control" name="block" id="block-filter" ref={input => this.inputFilters.block = input} onChange={() => this.filter()}>
                <option value="">Filter by Block</option>
                <option value="71">71</option>
                <option value="73">73</option>
                <option value="79">79</option>
              </select>
            </div>
            <div className="col-lg-6">
              <select className="form-control" name="type" id="type-filter" ref={input => this.inputFilters.type = input} onChange={() => this.filter()}>
                <option value="">Filter by Type</option>
                <option value="Facilitator">Facilitator</option>
                <option value="Startup">Startup</option>
              </select>
            </div>
          </div>
          <div className="directory-list">
            {
              this.state.companies.map((company, i) => (
                <div className="directory-item col-md-3" key={i}>
                  <img src={`/assets/images/companies/Block ${company.block}/${this.getSimpleCompanyName(company.name)}.png`} alt={company.name} />
                </div>
              ))
            }
          </div>
          <div className="pagination pull-right">
            {
              this.state.page > 3 ? (
                <button onClick={() => this.goToPage(1)}>1...</button>
              ) : ''
            }
            {
              _.range(pageStart, this.state.page).map((page, i) => (
                <button onClick={() => this.goToPage(page)} key={i}>{page}</button>
              ))
            }
            <button className="currentPage">{this.state.page}</button>
            {
              _.range(this.state.page + 1, pageEnd + 1 ).map((page, i) => (
                <button onClick={() => this.goToPage(page)} key={i}>{page}</button>
              ))
            }
            {
              this.state.page + 2 < lastPage ? (
                <div>
                  <button onClick={() => this.goToPage(this.state.page + 1)}>Next</button>
                  <button onClick={() => this.goToPage(lastPage)}>Last</button>
                </div>
              ) : ''
            }
          </div>
        </div>
      </div>
    );
  }
}

export default SectionDirectory;
