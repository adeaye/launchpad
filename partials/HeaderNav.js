import React from 'react';

import { Navbar,Nav,NavItem,NavDropdown,MenuItem,Image } from 'react-bootstrap';

class HeaderNav extends React.Component
{
	constructor(props)
	{
		super(props)
	}

	render()
	{
		return(
				<Navbar fixedTop fluid className="navbar-transparent" id="">
				    <Navbar.Header>
				      <Navbar.Brand>
				        <a href="#">
							<h4>Launchpad Ecosystem</h4>
				        </a>
				      </Navbar.Brand>
				    </Navbar.Header>
					<Nav pullRight>
						<NavItem><img src="assets/images/techsg-logo.svg" alt=""/></NavItem>
					</Nav>
				</Navbar>
			)
	}
}

export default HeaderNav;