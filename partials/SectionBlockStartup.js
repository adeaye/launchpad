import React from 'react';

import { Image,Grid,Row,Col,Button } from 'react-bootstrap';

class SectionBlockStartup extends React.Component
{
    render() {
        return(
            <div className="block-startup">
                <Grid>
                    <Row>
                        <Col md={4} xs={12}>
                            <div className="block-startup__content">
                                <div className="content-heading">
                                    <h4>Block 71</h4>
                                </div>
                                <Row>
                                    <Col xs={6}>
                                        <img src="assets/images/block-fasilitator.svg" alt=""/>
                                        <div className="count">36</div>
                                        <div className="block-name">Fasilitator</div>
                                    </Col>
                                    <Col xs={6}>
                                        <img src="assets/images/block-startup.svg" alt=""/>
                                        <div className="count">184</div>
                                        <div className="block-name">Startup</div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                        <Col md={4} xs={12}>
                            <div className="block-startup__content">
                                <div className="content-heading">
                                    <h4>Block 73</h4>
                                </div>
                                <Row>
                                    <Col xs={6}>
                                        <img src="assets/images/block-fasilitator.svg" alt=""/>
                                        <div className="count">3</div>
                                        <div className="block-name">Fasilitator</div>
                                    </Col>
                                    <Col xs={6}>
                                        <img src="assets/images/block-startup.svg" alt=""/>
                                        <div className="count">68</div>
                                        <div className="block-name">Startup</div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                        <Col md={4} xs={12}>
                            <div className="block-startup__content">
                                <div className="content-heading">
                                    <h4>Block 79</h4>
                                </div>
                                <Row>
                                    <Col xs={6}>
                                        <img src="assets/images/block-fasilitator.svg" alt=""/>
                                        <div className="count">25</div>
                                        <div className="block-name">Fasilitator</div>
                                    </Col>
                                    <Col xs={6}>
                                        <img src="assets/images/block-startup.svg" alt=""/>
                                        <div className="count">109</div>
                                        <div className="block-name">Startup</div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

export default SectionBlockStartup;