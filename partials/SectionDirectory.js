import React from 'react';

import companies from '../data/company';
import _ from 'lodash';

import { Nav,Pagination,NavDropdown,MenuItem,Grid,Row,Col,FormControl,FormGroup,InputGroup,Button } from 'react-bootstrap';

class SectionDirectory extends React.Component
{
    constructor(props) {
        super(props);
        this.itemPerPage = 12;
        this.state = {
          page: 1,
          listAll: companies,
          companies: companies.slice(0, this.itemPerPage),
        };
        this.inputFilters = {
          name: null,
          block: null,
          type:null,
        };
      }

      goToPage(page) {
        this.setState({
          page: page,
          companies: this.state.listAll.slice(this.itemPerPage * (page - 1), this.itemPerPage * page),
        });
      }

      setNewList(newList) {
        this.setState({
          page: 1,
          listAll: newList,
          companies: newList.slice(0, this.itemPerPage),
        });
      }

      filter(e) {
        const newList = companies.filter((company) =>
          company.name.toLowerCase().includes(this.inputFilters.name.value.toLowerCase()) &&
          company.type.includes(this.inputFilters.type.value) &&
          company.block.includes(this.inputFilters.block.value)
        );
        this.setNewList(newList);
      }

      getSimpleCompanyName(str) {
        return str.replace(/\s+/g, '-').toLowerCase().replace(/-pte.-ltd./g,'');
      }

      changeImage = (id)=>{
        $(`#${id}`).attr("src","assets/images/companies/not-found.png");
      }

    render() {
        const lastPage = Math.ceil(this.state.listAll.length/this.itemPerPage);
        const pageStart = (this.state.page - 2) < 1  ? 1 : (this.state.page - 2);
        const pageEnd = (this.state.page + 2) > lastPage  ? lastPage : (this.state.page + 2);
        return(
            <div className="directory">
                <Grid>
                    <section className="title">
                        <h4>Directory</h4>
                    </section>
                    <div className="directory__filter">
                        <Row>
                            <Col sm={4}>
                                <FormGroup>
                                    <InputGroup>
                                        <InputGroup.Addon><i className="fa fa-search" aria-hidden="true"></i></InputGroup.Addon>
                                        <input type="text" name="directory-search" id="directory-search" placeholder="Search..." ref={input => this.inputFilters.name = input} onChange={() => this.filter()} className={'form-control'} />
                                    </InputGroup>
                                </FormGroup>
                            </Col>
                            <Col sm={2}></Col>
                            <Col sm={6}>
                                <Nav pullRight>
                                    <div className="col-sm-6">
                                        <select className="form-control" name="block" id="block-filter" ref={input => this.inputFilters.block = input} onChange={() => this.filter()}>
                                            <option value="">Filter by Block</option>
                                            <option value="71">71</option>
                                            <option value="73">73</option>
                                            <option value="79">79</option>
                                        </select>
                                    </div>
                                    <div className="col-sm-6">
                                        <select className="form-control" name="type" id="type-filter" ref={input => this.inputFilters.type = input} onChange={() => this.filter()}>
                                            <option value="">Filter by Type</option>
                                            <option value="Facilitator">Facilitator</option>
                                            <option value="Startup">Startup</option>
                                        </select>
                                    </div>
                                </Nav>
                            </Col>
                        </Row>
                    </div>
                    <div className="directory__content">
                        <Row>
                            {this.state.companies.map((company, index) => (
                                <Col sm={6} md={3} key={index}>
                                    <div className="content__box">
                                        <img onError={()=>this.changeImage(`dir${company.No}`)} id={`dir${company.No}`} className="img-responsive images center-block" src={`/assets/images/companies/block ${company.block}/${this.getSimpleCompanyName(company.name)}.png`} alt={company.name}/>
                                        <div className="box__description">
                                            <div className="text">
                                                <div className="block">{company.block?company.block:'not found'}</div>
                                                <div className="type">{company.type?company.type:'not found'}</div>
                                                <div className="name">{company.name?company.name:'not found'}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            ))}
                        </Row>
                        {_.isEmpty(this.state.companies)?'':
                          <div className="pagination pull-right">
                            {
                                this.state.page > 3 ? (
                                        <button onClick={() => this.goToPage(1)}>1...</button>
                                    ) : ''
                            }
                            {
                                _.range(pageStart, this.state.page).map((page, i) => (
                                    <button onClick={() => this.goToPage(page)} key={i}>{page}</button>
                                ))
                            }
                            <button className="currentPage">{this.state.page}</button>
                            {
                                _.range(this.state.page + 1, pageEnd + 1 ).map((page, i) => (
                                    <button onClick={() => this.goToPage(page)} key={i}>{page}</button>
                                ))
                            }
                            {
                                this.state.page + 2 < lastPage ? (
                                        <button onClick={() => this.goToPage(this.state.page + 1)}>Next</button>
                                    ) : ''
                            }
                            {
                                this.state.page + 2 < lastPage ? (
                                        <button onClick={() => this.goToPage(lastPage)}>Last</button>
                                    ) : ''
                            }
                        </div>
                        }
                    </div>
                </Grid>
            </div>
        )
    }
}

export default SectionDirectory;