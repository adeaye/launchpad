import React from 'react';

import { Image,Grid,Row,Col,Button } from 'react-bootstrap';

class SectionFooter extends React.Component
{
    render(){
        return(
            <div className="bg-footer">
                <div className="footer__top">
                    <Grid>
                        <section className="title">
                            <h4>Powered By</h4>
                        </section>
                        <Row>
                            <Col sm={6}>
                                <div className="box-images">
                                    <img className="img-responsive center-block" src="assets/images/powered-nus.png" alt="Nus Enterprise"/>
                                </div>
                            </Col>
                            <Col sm={6}>
                                <div className="box-images">
                                    <img className="img-responsive center-block" src="assets/images/powered-wonderlabs.png" alt="Nus Enterprise"/>
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                </div>
                <div className="footer__bottom">
                    <p>Copyright &copy; 2017 Nus. All Right Reserved.</p>
                </div>
            </div>
        )
    }
}

export default SectionFooter;