import React from 'react';
import BlockMovement from '../data/blockMovement';
import { Image,Grid,Row,Col} from 'react-bootstrap';
import SectionSlider from './SectionSlider';
import NumberAnimate from '../sharedComponent/NumberAnimate';

class SectionProgress extends React.Component{
	constructor()
	{
		super();
		this.state = {
            counter: 0,
            block71:{
				fasilitator:0,
				startup:0,	
				},
			block73:{
				fasilitator:0,
				startup:0,
			},
			block79:{
				fasilitator:0,
				startup:0,
			},
			sliderValue:0,
			maxSlider:7,
        };
        this.startProgress = () => this._startProgress();

	}
	componentWillUnmount() {
        clearInterval(this.interval);
    }
    tick() {
        if (this.state.counter<=6) {
        	this.setState({
        		sliderValue:this.state.sliderValue+1,
	            counter: this.state.counter + 1
	        },()=>this.setMovement(this.state.counter-1));
        }
        else if(6<this.state.counter<=9)
        {
        	this.setState({
        		counter:this.state.counter+1
        	},()=>this._resetProgress(this.state.counter));
        }
    }
    setMovement(step)
    {
    	this.setState(BlockMovement[step]);
    }
    _startProgress(){
    	$('#play-button').addClass('hidden');
    	$('#sliderProgress').removeClass('hidden');
    	this.interval = setInterval(this.tick.bind(this), 1500);
    }

    _resetProgress(counter)
    {
    	if (counter === 9) {
    		clearInterval(this.interval);
        	this.setState({
        		counter:0,
        		sliderValue:0,
        		block71:{
					fasilitator:0,
					startup:0,	
					},
				block73:{
					fasilitator:0,
					startup:0,
				},
				block79:{
					fasilitator:0,
					startup:0,
				}
        	});

        	$('#play-button').removeClass('hidden');
        	$('#sliderProgress').addClass('hidden');
    	}
    }
	render()
	{
		return(
			<div id={'section-progress'}>
				<Grid>
					<Row>
						<Col md={10} mdOffset={1}>
							<Row className={'hasBorderBottom'}>
								<Col md={4}>
									<div className="block-status text-center">
										<div className="block-status__wrapper">
											<h3 className="block-status__number text-right">
												<NumberAnimate
												  value={this.state.block71.fasilitator}
												  speed={700}
												  ease='quintInOut' />
											</h3>
											<h4 className="block-status__text text-right">Fasilitator</h4>
										</div>
										<div className="block-status__wrapper">
											<h2 className="block-status__name">BLOCK 71</h2>
										</div>
										<div className="block-status__wrapper">
											<h3 className="block-status__number text-left">
												<NumberAnimate
												  value={this.state.block71.startup}
												  speed={700}
												  ease='quintInOut' />
											</h3>
											<h4 className="block-status__text text-left">Startup</h4>
										</div>
									</div>
									<div className={'block-status__image-wrapper'}>
										<Image src="/assets/images/block-71.svg" responsive className={'center-block'}/>
									</div>
								</Col>
								<Col md={4}>
									<div className="block-status text-center">
										<div className="block-status__wrapper">
											<h3 className="block-status__number text-right">
												<NumberAnimate
												  value={this.state.block73.fasilitator}
												  speed={700}
												  ease='quintInOut' />
											</h3>
											<h4 className="block-status__text text-right">Fasilitator</h4>
										</div>
										<div className="block-status__wrapper">
											<h2 className="block-status__name">BLOCK 73</h2>
										</div>
										<div className="block-status__wrapper">
											<h3 className="block-status__number text-left">
												<NumberAnimate
												  value={this.state.block73.startup}
												  speed={700}
												  ease='quintInOut' />
											</h3>
											<h4 className="block-status__text text-left">Startup</h4>
										</div>
									</div>
									<div className={'block-status__image-wrapper'}>
										<Image src="/assets/images/block-73.svg" responsive className={'center-block'}/>
									</div>
								</Col>
								<Col md={4}>
									<div className="block-status text-center">
										<div className="block-status__wrapper">
											<h3 className="block-status__number text-right">
												<NumberAnimate
												  value={this.state.block79.fasilitator}
												  speed={700}
												  ease='quintInOut' />
											</h3>
											<h4 className="block-status__text text-right">Fasilitator</h4>
										</div>
										<div className="block-status__wrapper">
											<h2 className="block-status__name">BLOCK 79</h2>
										</div>
										<div className="block-status__wrapper">
											<h3 className="block-status__number text-left">
												<NumberAnimate
												  value={this.state.block79.startup}
												  speed={700}
												  ease='quintInOut' />
											</h3>
											<h4 className="block-status__text text-left">Startup</h4>
										</div>
									</div>
									<div className={'block-status__image-wrapper'}>
										<Image src="/assets/images/block-79.svg" responsive className={'center-block'}/>
									</div>
								</Col>
							</Row>
						</Col>
					</Row>

					<SectionSlider
					maxSlider={this.state.maxSlider}
					sliderValue={this.state.sliderValue} />

				</Grid>
				<Image src="/assets/images/play-button.svg" responsive id={'play-button'} onClick={this.startProgress}/>

			</div>)
	}

}

export default SectionProgress;