import React from 'react';

import Slider from 'rc-slider';
import {Row,Col} from 'react-bootstrap';
const Handle = Slider.Handle;
import Tooltip from 'rc-tooltip';

const SectionSlider = ({maxSlider,sliderValue})=>{
	return(
		<Row id={'sliderProgress'} className={'hidden'}>
		    <Col  xs={3}>
		        <h2 className={ 'text-right'} style={{color: '#fff'}}>2015</h2>
		    </Col>
		    <Col  xs={6} style={{paddingTop: '25px'}}>
		        <Slider min={0} max={maxSlider} value={sliderValue} maximumTrackStyle={{ height: 12 }} minimumTrackStyle={{ backgroundColor: '#39d0fd', height: 12 }} handleStyle={{ boxShadow: '1px 3px 8px #39d0fd', height: 30, width: 30, marginLeft: -14, marginTop: -9, backgroundColor: '#ffffff', }} handle={handle} disabled />
		    </Col>
		    <Col xs={3}>
		        <h2 className={ 'text-left'} style={{color: '#fff'}}>2017</h2>
		    </Col>
		</Row>)
}

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={`${value}%`}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle {...restProps} />
    </Tooltip>
  );
};

export default SectionSlider;