import React from 'react';
import { Image,Grid,Row,Col,Tabs,Tab, Nav, NavItem } from 'react-bootstrap';
import BarChart from '../sharedComponent/BarChart';
import PieChart from '../sharedComponent/PieChart';

class SectionTab extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	render()
	{
		return(
			<div id={'section-tab'}>
				<Grid>
					<Tab.Container id={'tab-1'} defaultActiveKey={1} className={'tab-launchpad'}>
						<div>
							<Nav bsStyle="pills" justified>
							    <NavItem eventKey={1}>Growth</NavItem>
							    <NavItem eventKey={2}>Where are they now</NavItem>
							    <NavItem eventKey={3}>Detailed</NavItem>
							    <NavItem eventKey={4}>Profile of startup</NavItem>
							    <NavItem eventKey={5}>Work cloud</NavItem>
							</Nav>

						<Tab.Content>
							<Tab.Pane eventKey={1}>
								<Row>
									<Col md={4}>
										<BarChart
											titleText={'BLOCK 71'}
											xAxisCategories={['2015','2017']}
											series={[{
										        name: 'Startup',
										        data: [144,184]

										    }, {
										        name: 'Fasilitator',
										        data: [27,36]

										    }]}
										/>
									</Col>
									<Col md={4}>
										<BarChart
											titleText={'BLOCK 73'}
											xAxisCategories={['2015','2017']}
											series={[{
										        name: 'Startup',
										        data: [28,68]

										    }, {
										        name: 'Fasilitator',
										        data: [0,3]

										    }]}
										/>
									</Col>
									<Col md={4}>
										<BarChart
											titleText={'BLOCK 79'}
											xAxisCategories={['2015','2017']}
											series={[{
										        name: 'Startup',
										        data: [44,109]

										    }, {
										        name: 'Fasilitator',
										        data: [14,25]

										    }]}
										/>
									</Col>
								</Row>
							</Tab.Pane>
							<Tab.Pane eventKey={2}>
								<Row>
									<Col md={6} mdOffset={3}>
										<PieChart
										title={{style:{'display':'none'}}}
										seriesName={'Total'}
										legendItemWidth={200}
										seriesData={[{
							                name: 'Relocated away',
							                y: 47.5
							            }, {
							                name: 'Remained in same Block',
							                y: 34.3,
							                sliced: true,
							                selected: true
							            }, {
							                name: 'RIP (Inactive / Ceased Ops)',
							                y: 14.1
							            }, {
							                name: 'Moved to other blocks',
							                y: 4
							            }]}/>
									</Col>
								</Row>
							</Tab.Pane>
							<Tab.Pane eventKey={3}>
								<Row>
									<Col md={4}>
										<PieChart
										title={{text: 'BLOCK 71',
											style:{'color':'white','font-size':'18px','font-family':'Roboto Slab','font-weight':'bold'},y:30,margin:30}}
										seriesName={'Total'}
										legendItemWidth={150}
										seriesData={[{
							                name: 'Relocated away',
							                y: 47.5
							            }, {
							                name: 'Remained in same Block',
							                y: 34.3,
							                sliced: true,
							                selected: true
							            }, {
							                name: 'RIP',
							                y: 14.1
							            }, {
							                name: 'Moved within LP',
							                y: 4
							            }]}/>
									</Col>
									<Col md={4}>
										<PieChart
										title={{text: 'BLOCK 73',
											style:{'color':'white','font-size':'18px','font-family':'Roboto Slab','font-weight':'bold'},y:30,margin:30}}
										seriesName={'Total'}
										legendItemWidth={150}
										seriesData={[{
							                name: 'Relocated away',
							                y: 47.5
							            }, {
							                name: 'Remained in same Block',
							                y: 34.3,
							                sliced: true,
							                selected: true
							            }, {
							                name: 'RIP',
							                y: 14.1
							            }, {
							                name: 'Moved within LP',
							                y: 4
							            }]}/>
									</Col>
									<Col md={4}>
										<PieChart
										title={{text: 'BLOCK 79',
											style:{'color':'white','font-size':'18px','font-family':'Roboto Slab','font-weight':'bold'},y:30,margin:30}}
										seriesName={'Total'}
										legendItemWidth={150}
										seriesData={[{
							                name: 'Relocated away',
							                y: 47.5
							            }, {
							                name: 'Remained in same Block',
							                y: 34.3,
							                sliced: true,
							                selected: true
							            }, {
							                name: 'RIP',
							                y: 14.1
							            }, {
							                name: 'Moved within LP',
							                y: 4
							            }]}/>
									</Col>
								</Row>
							</Tab.Pane>
							<Tab.Pane eventKey={4}>
								<Row>
									<Col md={6}>
										<PieChart
											title={{text: 'Tenure in Launchpad',
												style:{'color':'white','font-size':'18px','font-family':'Roboto Slab','font-weight':'bold'},y:30,margin:30}}
											seriesName={'Total'}
											legendItemWidth={150}
											seriesData={[{
								                name: '< 15 Years',
								                y: 73,
								                sliced: true,
								                selected: true
								            }, {
								                name: '>= 15 Years',
								                y: 27
								            }]}/>
									</Col>
									<Col md={6}>
										<BarChart
											titleText={'Tenure in Launchpad'}
											xAxisCategories={['Block 71','Block 73','Block 79']}
											series={[{
										        name: '>= 15 Years',
										        data: [29,21,14]

										    }, {
										        name: '< 15 Years',
										        data: [71,79,86]

										    }]}
										/>
									</Col>
								</Row>
								<br/>
								<br/>
								<Row>
									<Col md={6}>
										<PieChart
											title={{text: 'Age of Startup',
												style:{'color':'white','font-size':'18px','font-family':'Roboto Slab','font-weight':'bold'},y:30,margin:30}}
											seriesName={'Total'}
											legendItemWidth={150}
											seriesData={[{
								                name: '< 2 Years',
								                y: 58,
								                sliced: true,
								                selected: true
								            }, {
								                name: '2-5 Years',
								                y: 27
								            }, {
								                name: '<5 Years',
								                y: 15
								            }]}/>
									</Col>
									<Col md={6}>
										<BarChart
											titleText={'Age of Startup'}
											xAxisCategories={['< 2 Years','2-5 Years','< 5 Years']}
											series={[{
										        name: '< 2 Years',
										        data: [50,45,78]

										    },
										    {
										        name: '2-5 Years',
										        data: [27,48,15]

										    },{
										        name: '< 5 Years',
										        data: [23,6,8]

										    }]}
										/>
									</Col>
								</Row>
							</Tab.Pane>
							<Tab.Pane eventKey={5}>
								<Row>
									<Col md={6}>
										<Image src="assets/images/word2015.png" responsive/>
										<h3 className="text-center" style={{color:'white'}}>2015</h3>
									</Col>
									<Col md={6}>
										<Image src="assets/images/word2017.png" responsive/>
										<h3 className="text-center" style={{color:'white'}}>2017</h3>
									</Col>
								</Row>
							</Tab.Pane>
						</Tab.Content>
						</div>
					</Tab.Container>
				</Grid>
			</div>
			)
	}
}

export default SectionTab;