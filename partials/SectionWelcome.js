import React from 'react';
import SectionProgress from './SectionProgress';

import { Image,Grid,Row,Col,Button } from 'react-bootstrap';

class SectionWelcome extends React.Component
{
    render() {
        return(
            <div className="bg-top">
                <Grid>
                    <Row>
                        <div className="intro">
                            <h1>The Densiest Startup Ecoystem</h1>
                            <h1>in The World</h1>
                        </div>
                        <div className="mouse-icon text-center visible-lg">
                            <div className="scroll"></div>
                        </div>
                        <section className="title">
                            <h4>Launchpad Growth & Statistics</h4>
                        </section>
                        <SectionProgress/>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default SectionWelcome;