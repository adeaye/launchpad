$(document).ready(function() {
    var header = $(".navbar-transparent");

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            header.removeClass('navbar-transparent').addClass('navbar-background');
        } else {
            header.removeClass('navbar-background').addClass('navbar-transparent');
        }
    });
});