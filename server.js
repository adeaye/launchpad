require('babel-register')({
    presets: ['react','es2015','stage-2']
});
var compression = require('compression');
// compile jsx to es5

var express = require('express');
var app = express();
var React = require('react');
var ReactDOMServer = require('react-dom/server');
var Frame = require('./Frame.jsx');

app.use(compression());
app.use(express.static('public'));

app.get('/', function(request, response) {
    var html = ReactDOMServer.renderToString(
        React.createElement(Frame)
    );
    response.send(html);
});


app.set('port', (process.env.PORT || 3000))
app.listen(app.get('port'), function() {
    console.log('http://localhost:' + app.get('port'));
});