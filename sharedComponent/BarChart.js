import React from 'react';
import ReactHighcharts from 'react-highcharts';

class BarChart extends React.Component{
	constructor()
	{
		super();
		this.state={
			config:{
				chart:{
			  		type:'column',
			  	},
			    title: {
			      text: ''
			    },
			    xAxis: {
			    	categories: []
			    },
			    yAxis: {
			      title: {
			        text: 'empty'
			      }
			    },
			    series: [
			    {
			      name: 'empty',
			      data: []
			    },
			    {
			      name: 'empty',
			      data: []	
			    }
			    ]
			}
		}
	}
	componentDidMount()
	{
		this.setState({
			config:{
				colors: ['#24b4df', '#c95369', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
				chart: {
			        type: 'column',
			        backgroundColor:'#0f2745',
			    },
			    credits: {
			        enabled: false
			    },
			    title: {
			        text: this.props.titleText,
			        style:{'color':'white','font-size':'18px','font-family':'Roboto Slab','font-weight':'bold'},
			        y:30,
			        margin:30
			    },
			    xAxis: {
			        categories: this.props.xAxisCategories,
			        crosshair: true,
			        tickColor: '#0f2745',
			        labels:{
			        	style:{'color':'#fff'},
			        },
			    },
			    yAxis: {
			        min: 0,
			        gridLineColor:'#0b1e36',
			        labels:{
			        	style:{'color':'#fff'},
			        },
			        title:{
			        	style:{'display':'none'},
			        },
			    },
			    legend: {
			        itemStyle: {
			            color: '#fff',
			        },
			    },
			    // tooltip: {
			    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			    //         '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
			    //     footerFormat: '</table>',
			    //     shared: true,
			    //     useHTML: true
			    // },
			    plotOptions: {
			        column: {
			            pointPadding: 0.2,
			            borderWidth: 0
			        }
			    },
			    series: this.props.series
			}
		})
	}
	render()
	{
		return(
			<ReactHighcharts  config = {this.state.config}></ReactHighcharts>)
	}
}

export default BarChart;