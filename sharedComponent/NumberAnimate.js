import React from 'react';
import eases from 'eases';
import PropTypes from 'prop-types';

class NumberAnimate extends React.Component{

    constructor(props)
    {
        super(props);
        const value = parseInt(props.value, 10);
        this.state={
            previousValue: value,
            displayValue: value
        }

        this.updateNumber = () => this._updateNumber();

    }

    static defaultProps = {
            speed: 500,
            ease: 'quintInOut',
            useLocaleString: false
        }
    timeout: null
    startAnimationTime: null

    componentWillReceiveProps(nextProps) {
        const value = parseInt(this.props.value, 10);

        if(parseInt(nextProps.value, 10) === value) return;

        this.setState({
            previousValue: this.state.displayValue
        });

        if (!isNaN(parseInt(this.props.delayValue, 10))) {
            this.delayTimeout = setTimeout(() => {
                this.startAnimationTime = (new Date()).getTime();
                this.updateNumber();
            }, this.props.delayValue);
        } else {
            this.startAnimationTime = (new Date()).getTime();
            this.updateNumber();
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.displayValue !== this.state.displayValue;
    }

    _updateNumber() {
        const value = parseInt(this.props.value, 10);

        const now = (new Date()).getTime();
        const elapsedTime = Math.min(this.props.speed, (now - this.startAnimationTime));
        const progress = eases[this.props.ease](elapsedTime / this.props.speed);

        const currentDisplayValue = Math.round((value - this.state.previousValue) * progress + this.state.previousValue);

        this.setState({
            displayValue: currentDisplayValue
        });

        if(elapsedTime < this.props.speed) {
            this.timeout = setTimeout(this.updateNumber, 16);
        } else {
            this.setState({
                previousValue: value
            });
        }
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
        clearTimeout(this.delayTimeout);
    }

    render() {
        const {className, useLocaleString, ...other} = this.props;
        const {displayValue} = this.state;
        let classes = 'react-number-easing';
        if(className) classes += ' ' + className;

        return (
            <span {...other} className={classes}>
                {useLocaleString ? displayValue.toLocaleString() : displayValue}
            </span>
        );
    }



}

NumberAnimate.propTypes={
    value: React.PropTypes.any.isRequired,
    speed: React.PropTypes.number,
    ease: React.PropTypes.oneOf(Object.keys(eases)),
    useLocaleString: React.PropTypes.bool,
    delayValue: React.PropTypes.number
}

export default NumberAnimate;