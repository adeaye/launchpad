import React from 'react';
import ReactHighcharts from 'react-highcharts';

class PieChart extends React.Component{
	constructor()
	{
		super();
		this.state={
			config:{
				colors: ['#24b4df', '#c95369', '#e563b0', '#5892c6', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
				chart: {
					backgroundColor:'#0f2745',
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        credits: {
			        enabled: false
			    },
		        title: {
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: false
		                },
		                showInLegend: true
		            }
		        },
		        legend: {
			        itemStyle: {
			            color: '#fff',
			        },
		            itemWidth:200,
		            itemMarginTop: 5,
        			itemMarginBottom: 5,
			    },
		        series: [{
		            name: '-',
		            colorByPoint: true,
		            data: []
		        }]
			}
		}
	}
	componentDidMount()
	{
		this.setState({
			config:{
				colors: ['#24b4df', '#c95369', '#e563b0', '#5892c6', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
				chart: {
					backgroundColor:'#0f2745',
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        credits: {
			        enabled: false
			    },
		        title: this.props.title,
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: false
		                },
		                showInLegend: true
		            }
		        },
		        legend: {
			        itemStyle: {
			            color: '#fff',
			        },
		            itemWidth:this.props.legendItemWidth,
		            itemMarginTop: 5,
        			itemMarginBottom: 5,
			    },
		        series: [{
		            name: this.props.seriesName,
		            colorByPoint: true,
		            data: this.props.seriesData,
		        }]
			}
		})
	}
	render()
	{
		return(
			<ReactHighcharts  config = {this.state.config}></ReactHighcharts>
			)
	}
}

export default PieChart;