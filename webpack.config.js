var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'eval', //dev purpose, in production comment it
  entry: 
{
 frame :'./public/assets/js/RenderFrame.jsx',
 // add      :'./nambah/add.jsx' 
},
  output: { 
    path      : path.join(__dirname, "public/assets/js/webpack-bundle"), 
    filename  : "[name].bundle.js" 
  },

  devServer: {
      inline: true,
      port: 8080
   },

  module: {
    loaders: [
      {
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',

      query: {
       presets:[ 'es2015', 'react', 'stage-2' ]
     }
   }
    ]
  },
plugins: [
  new webpack.DefinePlugin({
    'process.env': {
      // This has effect on the react lib size
      'NODE_ENV': JSON.stringify('development'),
    }
  }),
  new webpack.optimize.UglifyJsPlugin()
],
};