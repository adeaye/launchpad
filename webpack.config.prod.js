var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  entry: 
{
 frame :'./public/assets/js/RenderFrame.jsx'
 // add      :'./nambah/add.jsx' 
},
  output: { 
    path      : path.join(__dirname, "public/assets/js/webpack-bundle"), 
    filename  : "[name].bundle.js" 
  },
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true,
          presets: ['es2015', 'react','stage-2']
        }
      }
    ],
  },
plugins: [
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
  new webpack.optimize.UglifyJsPlugin({
        sourcemap: true,
      })
],
};